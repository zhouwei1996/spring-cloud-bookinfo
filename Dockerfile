# 这一行指定了基础镜像，使用的是包含 Gradle 7.1 和 JDK 8 的镜像，并给这个阶段起了一个别名为 "builder"。
FROM gradle:7.1-jdk8 AS builder
# 这里定义了一个构建参数 TARGET，默认值为 "gateway"。这个参数用于指定要构建的 Gradle 项目。
ARG TARGET=gateway
# 将当前目录下的所有文件复制到容器中的 /app/builder 目录。
COPY / /app/builder
# 将工作目录设置为 /app/builder。
WORKDIR /app/builder
# 运行 Gradle 命令构建指定的项目。
RUN gradle ${TARGET}:build

# 指定了基础镜像，使用的是包含 OpenJDK 8 的 Alpine 3.9 镜像
FROM openjdk:8-alpine3.9
# 同样是定义了构建参数 TARGET，默认值为 "gateway"
ARG TARGET=gateway

# 从第一个阶段的 "builder" 阶段中复制构建好的 JAR 文件到 /app 目录
COPY --from=builder /app/builder/${TARGET}/build/libs/ /app

# 将工作目录设置为 /app
WORKDIR /app

# 在容器启动时运行的命令，启动 Java 应用程序并执行 app.jar
CMD ["java", "-jar", "app.jar","--server.port=8080"]